﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatFiring : MonoBehaviour {

    InputController inputController;
    public GameObject cannonballPrefab;
    public float fireForce = 2000;
    public float rateOfFire = 2;
    public Transform firePoint;

    private bool canFire = true;
    private float timeOfLastFiring = 0;



    // Use this for initialization
    void Start () {
        inputController = GetComponent<InputController>();

        StartCannonballPool();

    }

    void StartCannonballPool()
    {
        Pool.CreatePool(cannonballPrefab, 2);
    }

    // Update is called once per frame
    void Update () {
        FireCycle();
        CanFireAgainReset();
    }

    void FireCycle()
    {
        if (inputController.CheckForFiring())
        {
            if (canFire)
            {
                Fire();
                canFire = false;
            }
        }

    }

    void CanFireAgainReset()
    {
        if (!canFire)
        {
            if (Time.time > timeOfLastFiring)
            {
                canFire = true;
            }
        }
    }

    void Fire()
    {
        GameObject newCannonball = Pool.Spawn(cannonballPrefab, firePoint.position, firePoint.rotation);

        Cannonball newCannonballScript = newCannonball.GetComponent<Cannonball>();

        newCannonballScript.Init(this);

        newCannonballScript.Activate();

        newCannonballScript.Fire(firePoint.forward * fireForce);

        timeOfLastFiring = Time.time + rateOfFire;
    }

    public Transform ReturnFirePoint()
    {
        return firePoint;
    }
}
