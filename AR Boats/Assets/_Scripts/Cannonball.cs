﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannonball : MonoBehaviour {

    private GameObject thisGameObject;
    private Collider cannonballCollider;
    private Transform cannonballTransform;
    private Rigidbody cannonballRigidbody;
    private Renderer cannonballRenderer;

    private BoatFiring boatFiring;

    public float lifeSpan = 5;

    // Use this for initialization
    void Start () {
    }

    public void Init(BoatFiring setBoatFiring)
    {
        boatFiring = setBoatFiring;

        thisGameObject = this.gameObject;
        cannonballCollider = this.GetComponent<Collider>();
        cannonballTransform = this.GetComponent<Transform>();
        cannonballRigidbody = this.GetComponent<Rigidbody>();
        cannonballRenderer = this.GetComponent<Renderer>();

        cannonballRigidbody.isKinematic = true;
        //cannonballCollider.enabled = false;
        //cannonballRenderer.enabled = false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        Explode();
    }

    void Explode()
    {
        Pool.Despawn(thisGameObject);
    }

    void SetPosition(Transform setPosition)
    {
        cannonballTransform.SetPositionAndRotation(setPosition.position, setPosition.rotation);
    }

    public void Fire(Vector3 fireVector)
    {
        cannonballRigidbody.AddForce(fireVector);

        Invoke("Explode", lifeSpan);
    }

    public void Activate()
    {
        cannonballRigidbody.isKinematic = false;

        SetPosition(boatFiring.ReturnFirePoint());
    }

    public void Deactivate()
    {
        cannonballRigidbody.isKinematic = true;

    }

}
