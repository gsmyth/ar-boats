﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardInput : InputController {

    public class DirectionKeys
    {
        public DirectionKeys(string setForward, string setBack, string setLeft, string setRight)
        {
            this.forwardKey = setForward;
            this.backKey = setBack;
            this.leftKey = setLeft;
            this.rightKey = setRight;
        }

        public string forwardKey = "UpArrow";
        public string backKey = "DownArrow";
        public string leftKey = "LeftArrow";
        public string rightKey = "RightArrow";
    }

    public DirectionKeys primaryDirectionKeys = new DirectionKeys("W","S","A","D");
    public DirectionKeys secondaryDirectionKeys = new DirectionKeys("UpArrow", "DownArrow", "LeftArrow", "RightArrow");

    private MouseLook mouseLook;


    private void Start()
    {
        mouseLook = GetComponentInChildren<MouseLook>();
    }

    private void Update()
    {

    }

    public override bool CheckForFiring()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            return true;
        }
        return false;
    }

    public override float ReturnThrustInput()
    {
        KeyCode forward = (KeyCode)System.Enum.Parse(typeof(KeyCode), primaryDirectionKeys.forwardKey);
        KeyCode back = (KeyCode)System.Enum.Parse(typeof(KeyCode), primaryDirectionKeys.backKey);

        if (Input.GetKey(forward))
        {
            return 1.0f;
        }

        if (Input.GetKey(back))
        {
            return -1.0f;
        }

        KeyCode altForward = (KeyCode)System.Enum.Parse(typeof(KeyCode), secondaryDirectionKeys.forwardKey);
        KeyCode altBack = (KeyCode)System.Enum.Parse(typeof(KeyCode), secondaryDirectionKeys.backKey);

        if (Input.GetKey(altForward))
        {
            return 1.0f;
        }

        if (Input.GetKey(altBack))
        {
            return -1.0f;
        }

        return 0.0f;

    }

    public override float ReturnTurnInput()
    {
        KeyCode left = (KeyCode)System.Enum.Parse(typeof(KeyCode), primaryDirectionKeys.leftKey);
        KeyCode right = (KeyCode)System.Enum.Parse(typeof(KeyCode), primaryDirectionKeys.rightKey);

        if (Input.GetKey(right))
        {
            return 1.0f;
        }

        if (Input.GetKey(left))
        {
            return -1.0f;
        }

        KeyCode altLeft = (KeyCode)System.Enum.Parse(typeof(KeyCode), secondaryDirectionKeys.leftKey);
        KeyCode altRight = (KeyCode)System.Enum.Parse(typeof(KeyCode), secondaryDirectionKeys.rightKey);

        if (Input.GetKey(altRight))
        {
            return 1.0f;
        }

        if (Input.GetKey(altLeft))
        {
            return -1.0f;
        }

        return 0.0f;
    }

    public override Vector2 ReturnFireDirection()
    {
        return base.ReturnFireDirection();
    }

    public override Vector2 ReturnFireDirectionNormalized()
    {
        return base.ReturnFireDirectionNormalized();
    }

    public override float ReturnFireMagnitude()
    {
        return base.ReturnFireMagnitude();
    }

}
