﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobileInput : InputController {

    [System.Serializable]
    public class StoreTouch
    {
        public Vector2 startTouch;
        public Vector2 endTouch;

        public bool updated = false;
    }

    public float touchInInchesForFullPower = 1;
    private float touchInPixelsForFullPower;

    private Rect leftSideOfScreen;
    private Rect rightSideOfScreen;

    private StoreTouch[] touches;

    private StoreTouch saveLook;

    private int movementFingerID = -1;
    private int firingFingerID = -1;

    // Use this for initialization
    void Start()
    {
        touches = new StoreTouch[10];
        for (int i = 0; i < touches.Length; i++)
        {
            touches[i] = new StoreTouch();
        }
        saveLook = new StoreTouch();
        UpdateSidesOfScreen();

        touchInPixelsForFullPower = Screen.dpi * touchInInchesForFullPower;

    }

    public void UpdateSidesOfScreen()
    {
        leftSideOfScreen = new Rect(Vector2.zero, new Vector2(Screen.width * 0.5f, Screen.height));
        rightSideOfScreen = new Rect(new Vector2(Screen.width * 0.5f, 0), new Vector2(Screen.height, Screen.width));
    }


    void OnGUI()
    {
        GUI.Label(leftSideOfScreen, "Left Side");
        GUI.Label(rightSideOfScreen, "Right Side");

    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < touches.Length; i++)
        {
            touches[i].updated = false;
        }

        foreach (Touch touch in Input.touches)
        {
            Debug.Log(touch.position);
            Vector2 positionInScreenPosition = touch.position;
            //positionInScreenPosition.y = Screen.height - positionInScreenPosition.y;
            //if (leftSideOfScreen.Contains(positionInScreenPosition))
            //{
                if (movementFingerID != -1 && movementFingerID != touch.fingerId)
                {
                    continue;
                }


                if (movementFingerID == -1)
                {
                    touches[touch.fingerId].startTouch = positionInScreenPosition;
                }

            movementFingerID = touch.fingerId;


            touches[touch.fingerId].endTouch = positionInScreenPosition;

                touches[touch.fingerId].updated = true;
            //}
        }
    }

    private void LateUpdate()
    {
        for(int i = 0; i < touches.Length; i++)
        {
            if (!touches[i].updated)
            {
                if (movementFingerID == i)
                {
                    movementFingerID = -1;
                    saveLook = new StoreTouch();
                    saveLook.startTouch = touches[i].startTouch;
                    saveLook.endTouch = touches[i].endTouch;

                }

                if (firingFingerID == i)
                {
                    firingFingerID = -1;
                }
            }
        }
    }

    public override float ReturnThrustInput()
    {
        if (movementFingerID == -1)
        {
            return 0;
        }

        Vector2 movementDirection = touches[movementFingerID].endTouch - touches[movementFingerID].startTouch;

        float thrust = Mathf.Clamp(Vector2.Dot(movementDirection, Vector2.up)/touchInPixelsForFullPower, -1, 1);

        //Debug.Log("Thrust: "+Vector2.Dot(movementDirection, Vector2.up)+" "+thrust);

        return thrust;
    }

    public override float ReturnTurnInput()
    {
        if (movementFingerID == -1)
        {
            return 0;
        }

        Vector2 movementDirection = touches[movementFingerID].endTouch - touches[movementFingerID].startTouch;

        float turn = Mathf.Clamp(Vector2.Dot(movementDirection, Vector2.right)/touchInPixelsForFullPower, -1, 1);

        //Debug.Log("Turn: "+Vector2.Dot(movementDirection, Vector2.right) + " " + turn);

        return turn;
    }

    //public override Vector2 ReturnFireDirection(int inputID = 0)
    //{
    //    float x = Input.GetAxisRaw("RightJoystickX");
    //    float y = Input.GetAxisRaw("RightJoystickY");

    //    return new Vector2(x, y);
    //}

    //public override Vector2 ReturnFireDirectionNormalized(int inputID = 0)
    //{
    //    Vector2 fireDirection = ReturnFireDirection();

    //    return fireDirection.normalized;
    //}

    //public override float ReturnFireMagnitude(int inputID = 0)
    //{
    //    Vector2 fireDirection = ReturnFireDirection();

    //    float magnitude = fireDirection.magnitude;

    //    return magnitude;
    //}

    //public override bool CheckForFiring(int inputID = 0)
    //{
    //    return Input.GetAxisRaw("RightTrigger") > 0.1f;
    //}
}
