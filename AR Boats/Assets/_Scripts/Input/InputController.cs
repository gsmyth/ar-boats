﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InputController : MonoBehaviour {

    public delegate void Fire();
    public event Fire onFire;
	
    public virtual float ReturnTurnInput()
    {
        return 0.0f;
    }

    public virtual float ReturnThrustInput()
    {
        return 0.0f;
    }

    public virtual Vector2 ReturnFireDirectionNormalized()
    {
        return Vector2.zero;
    }

    public virtual Vector2 ReturnFireDirection()
    {
        return Vector2.zero;
    }

    public virtual float ReturnFireMagnitude()
    {
        return 0.0f;
    }

    public virtual bool CheckForFiring()
    {
        return false;
    }

    protected void TriggerFire()
    {
        if (onFire != null)
            onFire();
    }
}
