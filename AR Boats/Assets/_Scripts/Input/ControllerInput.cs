﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerInput : InputController {

    public override float ReturnThrustInput()
    {
        float thrust = Input.GetAxisRaw("LeftJoystickY");

        return thrust;
    }

    public override float ReturnTurnInput()
    {
        float turn = Input.GetAxisRaw("LeftJoystickX");

        return turn;
    }

    public override Vector2 ReturnFireDirection()
    {
        float x = Input.GetAxisRaw("RightJoystickX");
        float y = Input.GetAxisRaw("RightJoystickY");

        return new Vector2(x, y);
    }

    public override Vector2 ReturnFireDirectionNormalized()
    {
        Vector2 fireDirection = ReturnFireDirection();

        return fireDirection.normalized;
    }

    public override float ReturnFireMagnitude()
    {
        Vector2 fireDirection = ReturnFireDirection();

        float magnitude = fireDirection.magnitude;

        return magnitude;
    }

    public override bool CheckForFiring()
    {
        return Input.GetAxisRaw("RightTrigger") > 0.1f;
    }

}
