﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Resize : MonoBehaviour {

    public Transform resizeObject;

    public Text displayText;

	// Use this for initialization
	void Start () {
        UpdateText();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Ones(int amount)
    {
        float changeAmount = 0.1f * amount;
        ResizeTransform(changeAmount);
    }

    public void Tens(int amount)
    {
        float changeAmount = 0.01f * amount;
        ResizeTransform(changeAmount);

    }

    public void Hundreds(int amount)
    {
        float changeAmount = 0.001f * amount;
        ResizeTransform(changeAmount);

    }

    private void ResizeTransform(float value)
    {
        if (resizeObject.localScale.x + value < 0)
        {
            return;
        }
        Vector3 newSize = resizeObject.localScale + value*Vector3.one;

        resizeObject.localScale = newSize;

        UpdateText();
    }

    void UpdateText()
    {
        displayText.text = (resizeObject.localScale.x).ToString("F3");
    }
}
