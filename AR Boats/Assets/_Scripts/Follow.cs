﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour {

    public Transform transformToFollow;
    private Transform thisTransform;

	// Use this for initialization
	void Start () {
        thisTransform = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 newPosition = transformToFollow.position;
        newPosition.y = thisTransform.position.y;
        thisTransform.position = newPosition;
	}
}
