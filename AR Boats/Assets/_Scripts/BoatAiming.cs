﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatAiming : MonoBehaviour
{

    InputController inputController;
    public Transform boatTurretTransform;

    // Use this for initialization
    void Start()
    {
        inputController = GetComponent<InputController>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 currentLookDirection = inputController.ReturnFireDirection();
        if (currentLookDirection.sqrMagnitude > 0.1f)
        {
            float angleToRotationAboutYAxis = Vector2.Angle(currentLookDirection, Vector2.up);
            Vector3 crossToCheckDirectionOfRotation = Vector3.Cross(currentLookDirection, Vector2.up);

            if (crossToCheckDirectionOfRotation.z > 0)
            {
                angleToRotationAboutYAxis = 360 - angleToRotationAboutYAxis;
            }

            Vector3 newLookDirection = new Vector3(currentLookDirection.x, 0f, currentLookDirection.y);

            Quaternion newLookRotation = Quaternion.AngleAxis(angleToRotationAboutYAxis, Vector3.up);

            boatTurretTransform.localRotation = Quaternion.Lerp(boatTurretTransform.localRotation, newLookRotation, Time.deltaTime * 10);
        }


        //Vector2 currentLookDirection = inputController.ReturnFireDirection();

        //float angleToRotate = Vector2.Angle(currentLookDirection, Vector2.up);
        //Vector3 cross = Vector3.Cross(currentLookDirection, Vector2.up);

        //if (cross.z > 0)
        //{
        //    angleToRotate = 360 - angleToRotate;
        //}

        //Vector3 newLocalEulerAngle = boatTurretTransform.localEulerAngles;
        //newLocalEulerAngle.y = angleToRotate;
        //boatTurretTransform.localEulerAngles = newLocalEulerAngle;

        //float angleToWorld = Vector3.Angle(boatTurretTransform.root.forward, Vector3.forward);
        //Vector3 crossAngleToWorld = Vector3.Cross(boatTurretTransform.root.forward, Vector3.forward);

        //if (crossAngleToWorld.y < 0)
        //{
        //    angleToWorld = 360 - angleToWorld;
        //}


        //if (currentLookDirection.x > 0.5f && currentLookDirection.y > 0.5f)
        //{
        //    newLocalEulerAngle = boatTurretTransform.localEulerAngles;
        //    newLocalEulerAngle.y = angleToWorld;
        //    boatTurretTransform.localEulerAngles = newLocalEulerAngle;
        //}


        //// This makes sure the turret only rotates around the y axis and shoots straight.
        //Vector3 newEulerAngle = boatTurretTransform.eulerAngles;
        //newEulerAngle.x = 0;
        //newEulerAngle.z = 0;

        //boatTurretTransform.eulerAngles = newEulerAngle;
    }
}


