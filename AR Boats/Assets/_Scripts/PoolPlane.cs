﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UnityEngine.XR.iOS
{
    public class PoolPlane : MonoBehaviour
    {



        public GameObject poolPlanePrefab;


        private Dictionary<string, PoolPlaneObject> planeAnchorMap;


        public PoolPlane()
        {
            planeAnchorMap = new Dictionary<string, PoolPlaneObject>();
            UnityARSessionNativeInterface.ARAnchorAddedEvent += AddAnchor;
            UnityARSessionNativeInterface.ARAnchorUpdatedEvent += UpdateAnchor;
            UnityARSessionNativeInterface.ARAnchorRemovedEvent += RemoveAnchor;

        }


        public void AddAnchor(ARPlaneAnchor arPlaneAnchor)
        {
            GameObject go = Pool.Spawn(poolPlanePrefab);
            //go.AddComponent<DontDestroyOnLoad>();  //this is so these GOs persist across scene loads
            PoolPlaneObject arpag = go.GetComponent<PoolPlaneObject>();
            if (arpag == null)
            {
                Pool.Despawn(go);
                return;
            }
            arpag.planeAnchor = arPlaneAnchor;
            arpag.thisGameObject = go;
            planeAnchorMap.Add(arPlaneAnchor.identifier, arpag);
        }

        public void RemoveAnchor(ARPlaneAnchor arPlaneAnchor)
        {
            if (planeAnchorMap.ContainsKey(arPlaneAnchor.identifier))
            {
                PoolPlaneObject arpag = planeAnchorMap[arPlaneAnchor.identifier];
                Pool.Despawn(arpag.thisGameObject);
                planeAnchorMap.Remove(arPlaneAnchor.identifier);
            }
        }

        public void UpdateAnchor(ARPlaneAnchor arPlaneAnchor)
        {
            if (planeAnchorMap.ContainsKey(arPlaneAnchor.identifier))
            {
                PoolPlaneObject arpag = planeAnchorMap[arPlaneAnchor.identifier];
                arpag.planeAnchor = arPlaneAnchor;
                planeAnchorMap[arPlaneAnchor.identifier] = arpag;

                UpdateSize(arpag, arPlaneAnchor);
            }
        }

        public void Destroy()
        {
            foreach (PoolPlaneObject arpag in GetCurrentPlaneAnchors())
            {
                Pool.Despawn(arpag.gameObject);
            }

            planeAnchorMap.Clear();
        }

        public List<PoolPlaneObject> GetCurrentPlaneAnchors()
        {
            return planeAnchorMap.Values.ToList();
        }

        public void UpdateSize(PoolPlaneObject poolPlane, ARPlaneAnchor arPlaneAnchor)
        {
            poolPlane.mainTransform.position = UnityARMatrixOps.GetPosition(arPlaneAnchor.transform);
            poolPlane.mainTransform.rotation = UnityARMatrixOps.GetRotation(arPlaneAnchor.transform);

            poolPlane.scaleTransform.localScale = new Vector3(arPlaneAnchor.extent.x, arPlaneAnchor.extent.y, arPlaneAnchor.extent.z);
            poolPlane.testTransform.localScale = new Vector3(arPlaneAnchor.extent.x, arPlaneAnchor.extent.y, arPlaneAnchor.extent.z);
            poolPlane.testTransform.localPosition = new Vector3(arPlaneAnchor.center.x, arPlaneAnchor.center.y, -arPlaneAnchor.center.z);

        }
    }
}