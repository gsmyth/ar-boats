﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;

public class PoolPlaneObject : MonoBehaviour {

        public GameObject thisGameObject;
        public ARPlaneAnchor planeAnchor;
        public Transform mainTransform;
        public Transform scaleTransform;
        public Transform testTransform;
}
