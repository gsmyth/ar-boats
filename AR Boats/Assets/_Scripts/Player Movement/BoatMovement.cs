﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatMovement : MonoBehaviour {

    InputController inputController;

    [System.Serializable]
    public class BoatMovementVariables
    {
        public float maxSpeed = 20;
        public float turnSpeed = 20;
        public float slowDownSpeed = 5;
        public float maxReverseSpeed = -10;
        public float accelerationRate = 1;
        public float decelerationRate = 5;
        public float counterMovementRate = 10;
        public float raiseBowTiltRate = 3;
        public float turnLeanTiltRate = 3;
    }

    public BoatMovementVariables boatMovementVariables;
    private Rigidbody thisRigidbody;
    private Transform thisTransform;
    public Transform directionTransform;
    private float currentSpeed = 0;
    [SerializeField]
    private float currentPower = 0;
    [SerializeField]
    private float targetPower = 0;
    private float currentTurnRate = 0;
    [SerializeField]
    private float resistance = 3;
    [SerializeField]
    private float correctTilt = 0.5f;

    void Awake () {
        inputController = GetComponent<InputController>();
        thisRigidbody = GetComponent<Rigidbody>();
        thisTransform = GetComponent<Transform>();
	}
	
	void Update () {
        GetInputForTurn();
        GetInputForCurrentSpeed();
	}

    private void FixedUpdate()
    {
        ApplyRotationsFromMovementOnBoat();
        ApplyTurning();
        ApplyMovement();
        ApplyCounterMovement();
    }

    public void ResetPosition()
    {
        thisTransform.position = Vector3.zero;
    }

    void GetInputForTurn()
    {
        currentTurnRate = inputController.ReturnTurnInput();
    }

    void GetInputForCurrentSpeed()
    {
        float VInput = inputController.ReturnThrustInput();

        // Target power is the speed we want to be.
        targetPower = VInput * boatMovementVariables.maxSpeed;

        // So this creates an acceleration curve based on your speed relative to the speed you want to be. 
        // It also has a slight boost with the VInput*10. This means it can hit max speed eventually, otherwise it will asymptope the max speed.
        // Normalized by deltaTime to make it smooth.
        currentPower += ((targetPower - currentPower) * boatMovementVariables.accelerationRate + VInput * 10) * Time.deltaTime;

        // This clamps the speed between the max and min power.
        currentPower = Mathf.Clamp(currentPower, boatMovementVariables.maxReverseSpeed, boatMovementVariables.maxSpeed);
    }

    void ApplyRotationsFromMovementOnBoat()
    {
        if (Mathf.Abs(targetPower) > 0)
        {
            thisRigidbody.AddTorque(thisTransform.right * (targetPower/boatMovementVariables.maxSpeed) * boatMovementVariables.raiseBowTiltRate * -1, ForceMode.Acceleration);
        }

        // This tilts for turning
        thisRigidbody.AddTorque(thisTransform.forward * currentTurnRate * boatMovementVariables.turnLeanTiltRate * -1, ForceMode.Acceleration);
    }

    void ApplyMovement()
    {
        Vector3 moveDirection = thisTransform.forward * currentPower;

        thisRigidbody.AddForce(moveDirection, ForceMode.Acceleration);

    }

    void ApplyTurning()
    {
        var targetRotation = (currentTurnRate * boatMovementVariables.turnSpeed * Mathf.Clamp((boatMovementVariables.maxSpeed / currentPower), 1.0f, 1.3f)) * Time.deltaTime;

        thisRigidbody.AddTorque(Vector3.up * targetRotation, ForceMode.Acceleration);


    }

    void ApplyCounterMovement()
    {
        // This applies a force against movement other than forward. Boats don't drift well. This is key to the feeling of boatiness
        thisRigidbody.AddForce(-Vector3.Project(thisRigidbody.velocity, thisTransform.right) * resistance);

        //Tilt Angle figures out the axis about which the boat is rotated around
        // Tilt Correct Mag is the magnitude of the force that will rotate the boat back (faster if it’s a large angle)
        Vector3 tiltAngle = Vector3.Cross(thisTransform.up, Vector3.up);
        float tiltCorrectMag = Vector3.Angle(thisTransform.up, Vector3.up);

        // This corrects tilt
        thisRigidbody.AddTorque(tiltAngle * tiltCorrectMag * tiltCorrectMag * correctTilt * Time.deltaTime, ForceMode.Acceleration);
    }

}


